import uuid from 'uuid/v1';
import {decorate, observable, action, computed} from 'mobx';

class TableStore {

  personList = [
    {name: "Aleksandr Stepovikov", salary: 2000, email: 'aleksandr@gmail.com', location: 'Belarus', phone: 123234, registerDate: new Date().toLocaleDateString(), id: uuid()},
    {name: "dsa dsa", salary: 1000, email: 'aleksandsadr@gmail.com', location: 'Belarus', phone: 7645, registerDate: new Date().toLocaleDateString(), id: uuid()},
    {name: "Aleksandr Stepovikov", salary: 1300, email: 'aleksandr@gmail.com', location: 'Belarus', phone: 123234, registerDate: new Date().toLocaleDateString(), id: uuid()},
  ];

  addPerson = (e) => {
    e['id'] = uuid();
    this.personList.push(e);
  };

  get averageSalary(){
    console.log(11111)
    return (this.personList.reduce((accumulator, second)=>(accumulator + second.salary), 0)/(this.personList.length)).toFixed(2)
  }

}
decorate(TableStore, {
  personList: observable,
  addPerson: action,
  averageSalary: computed
});
const tableStore = new TableStore();

export default tableStore;
