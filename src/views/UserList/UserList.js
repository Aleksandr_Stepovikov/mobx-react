import React from 'react';
import { makeStyles } from '@material-ui/styles';

import { UsersToolbar, UsersTable } from './components';
import tableStore from '../../store/tabelStore';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3)
  },
  content: {
    marginTop: theme.spacing(2)
  }
}));



const UserList = () => {

  const classes = useStyles();

  return (
    <div className={classes.root}>
      <UsersToolbar addUser={tableStore.addPerson} />
      <div className={classes.content}>
        <UsersTable personList={tableStore.personList} averageSalary={tableStore.averageSalary}/>
      </div>
    </div>
  );
};

export default UserList;
