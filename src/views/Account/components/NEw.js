import React from 'react'
import { observer } from "mobx-react"
import { observable, action } from "mobx"


class NEw extends React.Component{

  @observable value = 'Name';

  handleName = (ev)=>{
    this.value = ev.target.value
  };

  componentWillUpdate(s,n){
    console.log(this)
  }
  componentDidMount(){
    console.log(this)
  }
  componentDidUpdate(){
    console.log(this)
  }

  render(){
    return(
      <div>
        <input value={this.value} onChange={this.handleName}/>
        <div>{this.value}</div>
      </div>
    )
  }
}
export default observer(NEw)
